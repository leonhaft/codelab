'use strict';


var isInit = false;

var room = prompt('enter a room');

var socket = io.connect();
if (room !== '') {
    console.log('joining room:' + room);
    socket.emit('create or join', room);
}

socket.on('full', function(room) {
    console.log('room :' + room + ' is full');
});

socket.on('empty', function(room) {
    isInit = true;
    console.log('room:' + room + ' is empty');
});

socket.on('join', function(room) {
    console.log('making request join room:' + room);
    console.log('you are init');
});

socket.on('log', function(log) {
    console.log.apply(console, log);
})