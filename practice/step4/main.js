/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */

'use strict';

// Put variables in global scope to make them available to the browser console.
var sendChannel, receiveChannel;
var startButton = document.querySelector('#startButton');
var callButton = document.querySelector('#callButton');
var hangupButton = document.querySelector('#hangupButton');

startButton.disabled = false;
callButton.disabled = true;
hangupButton.disabled = true;
var pcConstraint;
var dataConstraint;

startButton.onclick = createConnection;
callButton.onclick = sendData;
hangupButton.onclick = closeDataChannels;

function createConnection() {
    var server = null;
    pcConstraint = null;
    dataConstraint = null;
    window.localPeerConnection = new RTCPeerConnection(server, pcConstraint);
    trace('Created local peer connection object localPeerConnection');
    try {
        sendChannel = localPeerConnection.createDataChannel('sendDataChannel', dataConstraint);
        trace('Created send data channel');
    } catch (error) {
        alert('Failed to create data channel. ' +
            'You need Chrome M25 or later with RtpDataChannel enabled');
        trace('createDataChannel() failed with exception: ' + e.message);
    }

    localPeerConnection.onicecandidate = getLocalCandidate;
    sendChannel.onopen = handDataChannelStateChange;
    sendChannel.onclose = handDataChannelStateChange;
    window.remotePeerConnection = new RTCPeerConnection(server, pcConstraint);
    trace('Created remote peer connection object remotePeerConnection');
    remotePeerConnection.onicecandidate = getRemoteCandidate;
    remotePeerConnection.ondatachannel = getReceiveChannel;
    localPeerConnection.createOffer(getLocalDescription, handleError);
    startButton.disabled = true;
    callButton.disabled = false;
    hangupButton.disabled = false;
}

function sendData() {
    var data = dataChannelSend.value;
    sendChannel.send(data);
    trace('send data:' + data);
}

function closeDataChannels() {
    trace('Closing data channels');
    sendChannel.close();
    trace('Closed data channel with label: ' + sendChannel.label);
    receiveChannel.close();
    trace('Closed data channel with label: ' + receiveChannel.label);
    localPeerConnection.close();
    localPeerConnection = null;
    remotePeerConnection.close();
    remotePeerConnection = null;
    trace('Closed peer connections');
    startButton.disabled = false;
    sendButton.disabled = true;
    closeButton.disabled = true;
    dataChannelSend.value = "";
    dataChannelReceive.value = "";
    dataChannelSend.disabled = true;
    dataChannelSend.placeholder = "Press Start, enter some text, then press Send.";
}

function getLocalDescription(desc) {
    localPeerConnection.setLocalDescription(desc);
    trace('Offer from localPeerConnection \n' + desc.sdp);
    remotePeerConnection.setRemoteDescription(desc);
    remotePeerConnection.createAnswer(getRemoteDescription, handleError);
}

function getRemoteDescription(desc) {
    remotePeerConnection.setLocalDescription(desc);
    trace('Answer from remotePeerConnection \n' + desc.sdp);
    localPeerConnection.setRemoteDescription(desc);
}


function getLocalCandidate(event) {
    trace('local ice callback');
    if (event.candidate) {
        remotePeerConnection.addIceCandidate(event.candidate);
        trace('Local ICE candidate: \n' + event.candidate.candidate);
    }
}

function getRemoteCandidate(evnet) {
    trace('remote ice callback');
    if (event.candidate) {
        localPeerConnection.addIceCandidate(event.candidate);
        trace('Local ICE candidate: \n' + event.candidate.candidate);
    }
}

function getReceiveChannel(event) {
    trace('Receive Channel Callback');
    receiveChannel = event.channel;
    receiveChannel.onmessage = handMessage;
    receiveChannel.onopen = handleReceiveChannelStateChange;
    receiveChannel.onclose = handleReceiveChannelStateChange;
}

function handMessage(event) {
    trace('Received message: ' + event.data);
    document.querySelector("#dataChannelReceive").value = event.data;
}

function handDataChannelStateChange() {
    var readyState = sendChannel.readyState;
    trace('Send channel state is: ' + readyState);
    if (readyState == "open") {
        dataChannelSend.disabled = false;
        dataChannelSend.focus();
        dataChannelSend.placeholder = "ces";
        callButton.disabled = false;
        hangupButton.disabled = false;
    } else {
        dataChannelSend.disabled = true;
        callButton.disabled = true;
        hangupButton.disabled = true;
    }
}

function handleReceiveChannelStateChange  () {
    var readyState = receiveChannel.readyState;
    trace('Receive channel state is: ' + readyState);
}

function handleError(error) {
    trace('error:' + error);
}