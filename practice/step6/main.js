'use strict';


var isInit = false;
var isChannelReady = false;
var isStarted = false;
var localStream = null;
var pc;
var remoteStream;
var turnReady = false;

var pc_config = { iceServers: [{ url: 'stun:stun.l.google.com:19302' }] };
var pc_constraints = { optional: { "DtlsSrtpKeyAgreement": true } };

// Set up audio and video regardless of what devices are present.
var sdpConstraints = {'mandatory': {
  'OfferToReceiveAudio':true,
  'OfferToReceiveVideo':true }};


var room=location.pathname.substring(1);
if(room==='')
{
    room='foo';
}else{
    
}


//var room = prompt('enter a room');

var socket = io.connect();
if (room !== '') {
    console.log('joining room:' + room);
    socket.emit('create or join', room);
}

socket.on('full', function(room) {
    console.log('room :' + room + ' is full');
});

socket.on('empty', function(room) {
    isInit = true;
    console.log('room:' + room + ' is empty');
});

socket.on('join', function(room) {
    console.log('making request join room:' + room);
    console.log('you are init');
    isChannelReady=true;
});

socket.on('joined',function (room) {
    console.log('The Peer has joined room:'+room);
})
socket.on('log', function(log) {
    console.log.apply(console, log);
})