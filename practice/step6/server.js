'use strict';
var nodSstatic = require('node-static');
var http = require('http');
var file = new (nodSstatic.Server)();
var app = http.createServer(function(req, res) {
    file.serve(req, res);
}).listen(2013);

var io = require('socket.io').listen(app);

io.sockets.on('connection', function(socket) {
    function log() {
        var array = [">>> Message from server: "];
        for (var i = 0; i < arguments.length; i++) {
            array.push(arguments[i]);
        }
        socket.emit('log', array);
    };

    socket.on('message', function(message) {
        log('got message:',message);
        socket.broadcast.emit('message',message);
    });
    
    socket.on('create or join',function (room) {
        var roomClientsLength=io.sockets.clients(room).length;
        log('room:'+room+'has '+roomClientsLength+' clients');
        log('request create or join room:'+room);
        if(roomClientsLength===0)
        {
            socket.join(room);
            socket.emit('created',room);
        }else if(roomClientsLength===1)
        {
            io.sockets.in(room).emit('join',room);
            socket.join(room);
            socket.emit('joined',room);
        }else 
        {
            socket.emit('full',room);
        }
        socket.emit('emit() : client '+socket.id+' joined room: '+room);
        socket.broadcast.emit('broadcast(): client '+socket.id+' joined room :'+ room);
    })
});