/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */

'use strict';

// Put variables in global scope to make them available to the browser console.
var localStream, localPeerConnection, remotePeerConnection;
var localVideo = document.querySelector('#gum-local');
var remoteVideo = document.querySelector('#gum-remote');

var startButton = document.querySelector('#startButton');
var callButton = document.querySelector('#callButton');
var hangupButton = document.querySelector('#hangupButton');

startButton.disabled = false;
callButton.disabled = true;
hangupButton.disabled = true;

startButton.onclick = start;
callButton.onclick = call;
hangupButton.onclick = hangup;

function getStream(stream) {
    trace('get remote stream');
    window.stream = stream;
    localStream = stream;
    localVideo.srcObject = stream;
    callButton.disabled = false;
}

function start() {
    trace('requesting local stream');
    startButton.disabled = true;
    navigator.getUserMedia({ video: true, audio: true }, getStream, function(error) {
        trace('get local stream error:' + error);
    });
}


function call() {
    callButton.disabled = true;
    hangupButton.disabled = false;
    trace('start call');

    if (localStream.getVideoTracks().length > 0) {
        trace('use video device:' + localStream.getVideoTracks()[0].label)
    }

    if (localStream.getAudioTracks().length > 0) {
        trace('use audio device:' + localStream.getAudioTracks()[0].label)
    }

    var server = null;
    localPeerConnection = new RTCPeerConnection(server);
    trace("Created local peer connection object localPeerConnection");
    localPeerConnection.onicecandidate = getLocalIceCandidate;
    remotePeerConnection = new RTCPeerConnection(server);
    trace("Created remote peer connection object remotePeerConnection");
    remotePeerConnection.onicecandidate = getRemoteIceCandidate;
    remotePeerConnection.onaddstream = getRemoteStream;
    localPeerConnection.addStream(localStream);
    trace("Added localStream to localPeerConnection");
    localPeerConnection.createOffer(getLocalDescripton, handError);
}

function getLocalDescripton(description) {
    localPeerConnection.setLocalDescription(description);
    trace("Offer from localPeerConnection: \n" + description.sdp);
    remotePeerConnection.setRemoteDescription(description);
    remotePeerConnection.createAnswer(getRemoteDescription, handError);
}

function getRemoteDescription(description) {
    remotePeerConnection.setLocalDescription(description);
    trace("Answer from remotePeerConnection: \n" + description.sdp);
    localPeerConnection.setRemoteDescription(description);
}

function hangup() {
    trace('ending call');
    hangupButton.disabled = true;

    localPeerConnection.close();
    remotePeerConnection.close();
    localPeerConnection = null;
    remotePeerConnection = null;
    callButton.disabled = false;
}


function getRemoteStream(event) {
    remoteVideo.srcObject = event.stream;
    trace("Received remote stream");
}

function getLocalIceCandidate(event) {
    if (event.candidate) {
        remotePeerConnection.addIceCandidate(new RTCIceCandidate(event.candidate));
        trace("Local ICE candidate: \n" + event.candidate.candidate);
    }
}

function getRemoteIceCandidate(event) {
    if (event.candiate) {
        localPeerConnection.addIceCandidate(new RTCIceCandidate(event.candidate));
        trace("Remote ICE candidate: \n " + event.candidate.candidate);
    }
}

function handError(error) {
    trace('error:' + error);
}
